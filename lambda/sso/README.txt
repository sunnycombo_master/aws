Set serverless for express

#############################################################################
#                         Basic Config                                      #
#############################################################################
- command: serverless -t aws-nodejs --path pathname
- config serverless.yml
    *******************************************************
    ex:
    service: sso
    provider:
    name: aws
    runtime: nodejs8.10
    stage: dev
    region: ap-southeast-1
    functions:
    sso:
        handler: app.handler
        events:
        - http: ANY /
        - http: 'ANY {proxy+}'

    plugins:
    - serverless-offline
    *******************************************************
- command: npm init -y
- command: npm i -s body-parser cors express mongoose serverless-http
- create app.js
    *******************************************************
    ex:
    const serverless = require("serverless-http");
    const express = require("express");
    const bodyParser = require("body-parser");
    const app = express();

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.get('/login', (req, res) => {
        res.status(200).send('Hello World Login');
    });

    module.exports.handler = serverless(app);
    *******************************************************
- command: npm install serverless-offline --save-dev //for run server in local
- command: serverless offline start | sls offline stop //start serverless offline


Docs ref : https://www.npmjs.com/package/serverless-offline