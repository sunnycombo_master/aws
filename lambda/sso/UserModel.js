//const {connect,mongoose} = require('./connect_db')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    name: String,
    password: String,
})

const UserCollection = async () => {
   return await mongoose.model('users', userSchema)
}

module.exports.UserCollection = UserCollection