const mongoose = require('mongoose');

const connect = async() => {
    await mongoose.connect('mongodb://178.128.91.236:27017/baania_sso');
};

module.exports = {
    connect
};